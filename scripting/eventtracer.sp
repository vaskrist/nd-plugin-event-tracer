#include <sourcemod>

#define TRACE(%0);			LogToFile(traceFile, %0);
#define TRACE_SERVER(%0);	PrintToServer(%0);LogToFile(traceFile, %0);

public Plugin:myinfo = 
{
	name = "Event Tracer Plugin",
	author = "Vaskrist",
	description = "Logs all events to SourceMod logs",
	version = "1.0",
	url = ""
}

enum ParameterType
{
	NONE,
	BOOL,
	INT,
	FLOAT,
	STRING
}

enum EventInfo
{
	String:eventName[64],
	String:parameters[250],
}

new eventInfos[150][EventInfo];
new eventInfosCount;

new String:traceFile[256];

public OnPluginStart()
{
	BuildPath(Path_SM, traceFile, sizeof(traceFile), "logs/event_trace.log");
	FillEventInfo();
	HookAllEvents();
}

HookAllEvents() 
{
	for (new i = 0; i < eventInfosCount; i++)
	{
		if (HookEventEx(eventInfos[i][eventName], LogEvent, EventHookMode_Post))
		{
			// LogMessage("DEBUG> LogAll: event '%s' hooked", eventInfos[i][eventName]);
		}
		else
		{
			TRACE_SERVER("Event '%s' doesn't exist", eventInfos[i][eventName]);
		}	
	}
}

NewEventInfo(info[EventInfo], String:name[], String:params[])
{
	strcopy(info[eventName], sizeof(info[eventName]), name);
	strcopy(info[parameters], sizeof(info[parameters]), params);
	
	return info;
}

ParameterType:StringToParameterType(String:param[])
{
	return 
		strcmp(param, "string") == 0 ? STRING :
		strcmp(param, "float") == 0 ? FLOAT :
		strcmp(param, "bool") == 0 ? BOOL :
		strcmp(param, "short") == 0 ? INT :
		strcmp(param, "long") == 0 ? INT : INT;
}

public LogEvent(Handle:event, const String:name[], bool:dontBroadcast)
{
	new String:buffer[1024] = "";
	
	new interface[EventInfo];
	// find the correct EventInfo
	for (new i = 0; i < eventInfosCount; i++)
	{
		if (strcmp(name, eventInfos[i][eventName]) == 0)
		{
			interface = eventInfos[i];
			Format(buffer, sizeof(buffer), "Event %s fired!", name);
			break;
		}
	}
	
	if (event != INVALID_HANDLE)
	{
		// get event description from EventInfo
		decl String:params[10][25];
		new parCount = ExplodeString(interface[parameters], ";", params, sizeof(params), sizeof(params[]));
		
		if (parCount > 0)
		{
			Format(buffer, sizeof(buffer), "%s Parameters:", buffer);
		}
		
		for (new i = 0; i < parCount; i++)
		{
			// LogMessage("DEBUG> LogEvent: parameter string '%s'", params[i]);
			
			decl String:param[2][25];
			ExplodeString(params[i], ":", param, sizeof(param), sizeof(param[]));
			
			new ParameterType:parType = StringToParameterType(param[0]);
			
			// LogMessage("DEBUG> LogEvent: type: %s, parameter '%s' -> %d", param[0], param[1], parType);
			// add the parameters to the message
			switch(parType)
			{
				case STRING:
				{
					new String:value[30];
					GetEventString(event, param[1], value, sizeof(value));
					// LogMessage("DEBUG> LogEvent: value: %s", value);
					Format(buffer, sizeof(buffer), "%s %s = %s", buffer, param[1], value);
				}
				case FLOAT:
				{
					new Float:value = GetEventFloat(event, param[1]);
					// LogMessage("DEBUG> LogEvent: value: %f", value);
					Format(buffer, sizeof(buffer), "%s %s = %f", buffer, param[1], value);
				}
				case BOOL:
				{
					new bool:value = GetEventBool(event, param[1]);
					// LogMessage("DEBUG> LogEvent: value: %d", value);
					Format(buffer, sizeof(buffer), "%s %s = %d", buffer, param[1], value);
				}
				case INT:
				{
					new value = GetEventInt(event, param[1]);
					// LogMessage("DEBUG> LogEvent: value: %d", value);
					Format(buffer, sizeof(buffer), "%s %s = %d", buffer, param[1], value);
				}
			}
		}
	}
	
	// print the message
	TRACE(buffer);
}

FillEventInfo()
{
	new i;

	// Generic Source Events
	NewEventInfo(eventInfos[i++], "team_info", "byte:teamid;string:teamname");
	NewEventInfo(eventInfos[i++], "team_info", "byte:teamid;short:score");
	NewEventInfo(eventInfos[i++], "teamplay_broadcast_audio", "byte:team;string:sound");
	NewEventInfo(eventInfos[i++], "player_team", "short:userid;byte:team;byte:oldteam;bool:disconnect;bool:autoteam;bool:silent;string:name");
	NewEventInfo(eventInfos[i++], "player_class", "short:userid;string:class");
	NewEventInfo(eventInfos[i++], "player_chat", "bool:teamonly;short:userid;string:text");
	NewEventInfo(eventInfos[i++], "player_score", "short:userid;short:kills;short:deaths;short:score");
	NewEventInfo(eventInfos[i++], "player_spawn", "short:userid");
	NewEventInfo(eventInfos[i++], "player_shoot", "short:userid;byte:weapon;byte:mode");
	NewEventInfo(eventInfos[i++], "player_use", "short:userid;short:entity");
	NewEventInfo(eventInfos[i++], "player_changename", "short:userid;string:oldname;string:newname");
	NewEventInfo(eventInfos[i++], "player_hintmessage", "string:hintmessage");
	NewEventInfo(eventInfos[i++], "game_newmap", "string:mapname");
	NewEventInfo(eventInfos[i++], "game_start", "long:roundslimit;long:timelimit;long:fraglimit;string:objective");
	NewEventInfo(eventInfos[i++], "game_end", "byte:winner");
	NewEventInfo(eventInfos[i++], "round_start", "long:timelimit;long:fraglimit;string:objective");
	NewEventInfo(eventInfos[i++], "round_end", "byte:winner;byte:reason;string:message");
	NewEventInfo(eventInfos[i++], "game_message", "byte:target;string:text");
	NewEventInfo(eventInfos[i++], "break_breakable", "long:entindex;short:userid;byte:material");
	NewEventInfo(eventInfos[i++], "break_prop", "long:entindex;short:userid");
	NewEventInfo(eventInfos[i++], "entity_killed", "long:entindex_killed;long:entindex_attacker;long:entindex_inflictor;long:damagebits");
	NewEventInfo(eventInfos[i++], "bonus_updated", "short:numadvanced;short:numbronze;short:numsilver;short:numgold");
	NewEventInfo(eventInfos[i++], "achievement_earned", "string:achievement_name;short:cur_val;short:max_val");
	NewEventInfo(eventInfos[i++], "physgun_pickup", "long:entindex");
	NewEventInfo(eventInfos[i++], "flare_ignite_npc", "long:entindex");

	// Generic Source Server Events
	NewEventInfo(eventInfos[i++], "ragdoll_dissolved", "long:entindex");
	NewEventInfo(eventInfos[i++], "server_spawn", "string:hostname;string:address;string:ip;string:port;string:game;string:mapname;long:maxplayers;string:os;bool:dedicated;bool:password");
	NewEventInfo(eventInfos[i++], "server_shutdown", "string:reason");
	NewEventInfo(eventInfos[i++], "server_cvar", "string:cvarname;string:cvarvalue");
	NewEventInfo(eventInfos[i++], "server_message", "string:text");
	NewEventInfo(eventInfos[i++], "server_addban", "string:name;string:userid;string:networkid;string:ip;string:duration;string:by;bool:kicked");
	NewEventInfo(eventInfos[i++], "server_removeban", "string:networkid;string:ip;string:by");
	NewEventInfo(eventInfos[i++], "player_connect", "string:name;byte:index;short:userid;string:networkid;string:address;short:bot");
	NewEventInfo(eventInfos[i++], "player_info", "string:name;byte:index;short:userid;string:networkid;bool:bot");
	NewEventInfo(eventInfos[i++], "player_disconnect", "short:userid;string:reason;string:name;string:networkid;short:bot");
	NewEventInfo(eventInfos[i++], "player_activate", "short:userid");
	NewEventInfo(eventInfos[i++], "player_say", "short:userid;string:text");
		
	// Nuclear dawn events
	NewEventInfo(eventInfos[i++], "player_death", "short:userid;short:attacker;string:weapon;short:weaponid;long:damagebits;long:customkill;short:priority");
//	NewEventInfo(eventInfos[i++], "player_hurt", "short:userid;short:attacker;short:health;string:weapon;short:dmg_health;byte:hitgroup;long:type");
	NewEventInfo(eventInfos[i++], "player_changeclass", "short:userid;short:class;short:subclass");
	NewEventInfo(eventInfos[i++], "spec_target_updated", "");
	NewEventInfo(eventInfos[i++], "achievement_earned", "short:player;short:achievement");
	NewEventInfo(eventInfos[i++], "player_spawned_at_tgate", "short:userid");
	NewEventInfo(eventInfos[i++], "structure_death", "short:type;short:team;short:entindex;short:attacker;string:weapon");
	NewEventInfo(eventInfos[i++], "structure_damage_sparse", "short:entindex;short:ownerteam;bool:bunker;local:userid;bool:nosound");
	NewEventInfo(eventInfos[i++], "structure_sold", "short:type;short:ownerteam");
	NewEventInfo(eventInfos[i++], "structure_fully_auto_repaired", "short:type;short:entindex;short:ownerteam");
	NewEventInfo(eventInfos[i++], "structure_fully_man_repaired", "short:type;short:entindex;short:ownerteam;local:userid");
	NewEventInfo(eventInfos[i++], "structure_built", "short:entindex");
	NewEventInfo(eventInfos[i++], "structure_power_outage", "short:entindex");
	NewEventInfo(eventInfos[i++], "resource_start_capture", "short:userid;short:entindex;short:type;short:capteam;short:ownerteam");
	NewEventInfo(eventInfos[i++], "resource_end_capture", "short:userid;short:entindex");
	NewEventInfo(eventInfos[i++], "resource_break_capture", "short:entindex;float:time_remaining");
	NewEventInfo(eventInfos[i++], "resource_extract", "short:entindex;short:amount");
	NewEventInfo(eventInfos[i++], "resource_captured", "short:entindex;short:type;short:team");
	NewEventInfo(eventInfos[i++], "power_requested", "short:teamid;short:entid");
	NewEventInfo(eventInfos[i++], "power_removed", "short:teamid;short:entid");
	NewEventInfo(eventInfos[i++], "power_updated", "");
	NewEventInfo(eventInfos[i++], "enter_pregame", "");
	NewEventInfo(eventInfos[i++], "enter_vehicle", "short:userid;short:seat;short:entidx");
	NewEventInfo(eventInfos[i++], "exit_vehicle", "short:userid;short:seat;short:entidx");
	NewEventInfo(eventInfos[i++], "show_freezepanel", "short:killer");
	NewEventInfo(eventInfos[i++], "hide_freezepanel", "");
	NewEventInfo(eventInfos[i++], "freezecam_started", "");
	NewEventInfo(eventInfos[i++], "spawn_map_changed", "short:teamid");
	NewEventInfo(eventInfos[i++], "overviewmap_key_released", "");
	NewEventInfo(eventInfos[i++], "slot_key_pressed", "short:slot");
	NewEventInfo(eventInfos[i++], "ironsight_key_pressed", "");
	NewEventInfo(eventInfos[i++], "ability_key_pressed", "");
//	NewEventInfo(eventInfos[i++], "weapon_reload", "short:userid;bool:manual");
//	NewEventInfo(eventInfos[i++], "weapon_fire_at_40", "short:userid;string:weapon;short:weaponid;short:count");
	NewEventInfo(eventInfos[i++], "entity_visible", "short:userid;long:subject;string:classname;string:entityname");
	NewEventInfo(eventInfos[i++], "player_left_bunker_building", "short:userid;short:entidx");
	NewEventInfo(eventInfos[i++], "player_entered_bunker_building", "short:userid;short:entidx");
	NewEventInfo(eventInfos[i++], "player_entered_commander_mode", "short:userid");
	NewEventInfo(eventInfos[i++], "player_enter_commander_mode_failed", "short:userid");
	NewEventInfo(eventInfos[i++], "player_left_commander_mode", "short:userid");
	NewEventInfo(eventInfos[i++], "player_restocked_from_supply_struct", "short:userid;short:entidx");
	NewEventInfo(eventInfos[i++], "player_opened_armoury_menu", "short:userid");
	NewEventInfo(eventInfos[i++], "commander_move_2d", "1:local;short:userid");
	NewEventInfo(eventInfos[i++], "commander_move_z", "1:local;short:userid");
	NewEventInfo(eventInfos[i++], "commander_minimap_move", "1:local;short:userid");
	NewEventInfo(eventInfos[i++], "player_exited_tgate", "short:userid");
	NewEventInfo(eventInfos[i++], "commander_left_valid_area", "short:userid");
	NewEventInfo(eventInfos[i++], "commander_entered_valid_area", "short:userid");
	NewEventInfo(eventInfos[i++], "round_win", "short:team;short:type");
	NewEventInfo(eventInfos[i++], "transport_gate_created", "short:teamid;short:entindex");
	NewEventInfo(eventInfos[i++], "last_tgate_destroyed", "short:teamid");
	NewEventInfo(eventInfos[i++], "player_stats_updated", "bool:forceupload");
	NewEventInfo(eventInfos[i++], "failed_to_build", "1:local;short:userid;short:reason");
	NewEventInfo(eventInfos[i++], "commander_hint_closed", "1:local");
	NewEventInfo(eventInfos[i++], "commander_selected_structure", "1:local;short:type;short:entindex");
	NewEventInfo(eventInfos[i++], "commander_flash_assembler_start", "1:local");
	NewEventInfo(eventInfos[i++], "commander_flash_assembler_stop", "1:local");
	NewEventInfo(eventInfos[i++], "commander_start_build_mode", "1:local;short:type");
	NewEventInfo(eventInfos[i++], "commander_flash_supply_icon_start", "1:local");
	NewEventInfo(eventInfos[i++], "commander_flash_supply_icon_stop", "1:local");
	NewEventInfo(eventInfos[i++], "commander_start_structure_build", "short:team;short:type");
	NewEventInfo(eventInfos[i++], "promoted_to_commander", "short:userid;short:teamid");
	NewEventInfo(eventInfos[i++], "timeleft_10m", "1:local");
	NewEventInfo(eventInfos[i++], "timeleft_5m", "1:local");
	NewEventInfo(eventInfos[i++], "timeleft_1m", "1:local");
	NewEventInfo(eventInfos[i++], "timeleft_30s", "1:local");
	NewEventInfo(eventInfos[i++], "timeleft_10s", "1:local");
	NewEventInfo(eventInfos[i++], "timeleft_5s", "1:local");
	NewEventInfo(eventInfos[i++], "ach_hospital_duty_progress", "short:progress");
	NewEventInfo(eventInfos[i++], "ach_high_maintenance_progress", "short:progress");
	NewEventInfo(eventInfos[i++], "forward_spawn_created", "short:teamid");
	NewEventInfo(eventInfos[i++], "research_complete", "short:teamid;short:researchid");
	NewEventInfo(eventInfos[i++], "show_annotation", "float:worldPosX;float:worldPosY;float:worldPosZ;float:worldNormalX;float:worldNormalY;float:worldNormalZ;long:id;string:text;float:lifetime;long:visibilityBitfield;long:follow_entindex;bool:show_distance;string:play_sound;bool:show_effect");
	NewEventInfo(eventInfos[i++], "hide_annotation", "long:id");
	NewEventInfo(eventInfos[i++], "end_training_session", "string:course;bool:successful");
	NewEventInfo(eventInfos[i++], "nav_blocked", "long:area;bool:blocked");
	NewEventInfo(eventInfos[i++], "nav_generate", "");
	NewEventInfo(eventInfos[i++], "medkit_heal", "1:local;local:entindex;local:ownerid;local:userid;local:amount");
	NewEventInfo(eventInfos[i++], "emp_structure", "1:local;local:entindex;local:userid");
	NewEventInfo(eventInfos[i++], "setup_compass_rotation", "long:rotation");
	NewEventInfo(eventInfos[i++], "tgate_use", "short:userid");
	NewEventInfo(eventInfos[i++], "attacked_radarkit", "short:userid");

	
	eventInfosCount = i;
}
